from flask import Flask, jsonify, request

app = Flask(__name__)

# Initialising Global Variables
visitor_count = 0
customer_details = [{'cust_id':1001, 'cust_name':'Urmila M', 'cust_policy':'GOLD' }, 
                    {'cust_id':1002, 'cust_name':'Sakshi S', 'cust_policy':'SILVER' },
                    {'cust_id':1003, 'cust_name':'Kajal A', 'cust_policy':'PLATINUM' }
                    ]

# Start of API Definitions
@app.route('/deep-api/v1/GetAppName', methods=['GET'])
def GetAppName():
  global visitor_count
  visitor_count = visitor_count + 1
  print("Sending AppName. Visitor Count: {0}".format(visitor_count))
  api_response = {'AppName': 'Deep-Docker API App', 'api_base': 'GetAppName','visitor_count':visitor_count, 'response_status': 'OK' }
  return jsonify(api_response)

@app.route('/deep-api/v1/GetCustDetails', methods=['GET'])
def GetCustDetails():
  global visitor_count
  global customer_details
  visitor_count = visitor_count + 1
  print("Sending Customer Details. Visitor Count: {0}".format(visitor_count))
  #api_response = {'api_base': 'GetCustDetails', 'visitor_count': visitor_count, 'response_status': 'OK'}
  #return jsonify(api_response)
  return jsonify(customer_details)

@app.route('/deep-api/v1/AddCustDetails', methods=['POST'])
def AddCustomerDetails():
  global visitor_count
  global customer_details
  # json request format is: {'cust_id':1003, 'cust_name':'Kajal A', 'cust_policy':'PLATINUM' }
  customer_details.append(request.get_json())
  visitor_count = visitor_count + 1
  print("AddCustDetails - New Record is: \n{0}".format(request.get_json()))
  print("Adding Customer Details. Visitor Count: {0}".format(visitor_count))
  api_response = {'api_base': 'AddCustDetails', 'visitor_count': visitor_count, 'response_status': 'OK'}
  return jsonify(api_response)


if __name__ == "__main__":
  app.run(host='0.0.0.0')
